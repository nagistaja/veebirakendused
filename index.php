<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Notepad</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>

<body>
<div class="container">
  <div class="row input-form">
    <form name="newnote" method ="post">
      <h3>Add a note:</h3>
      <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Name" maxlength="30" required>
      </div>
      <div class="form-group">
        <label for="note">Note:</label>
        <textarea class="form-control" rows="5" id="note" name="note" placeholder="Enter note (maximum length 250 characters)" maxlength="250" required></textarea>
      </div>
      <button type="submit" name="querySubmit" class="btn btn-default">Submit</button>
    </form>
  </div>

  <?php
  require_once ('db.php');

  connectDB();
  global $connection;

  if (isset($_POST['name']) && isset($_POST['note'])) {
    $name = mysqli_real_escape_string($connection, htmlspecialchars($_POST['name']));
    $note = mysqli_real_escape_string($connection, htmlspecialchars($_POST['note']));
    $sql = "INSERT INTO nagistaja_eksam (NAME, NOTE) VALUES ('$name', '$note')";
    if (mysqli_query($connection, $sql)) {
      echo "<div class=\"row message success\">Note added!</div>";
    } else {
      echo "<div class=\"row message fail\">Error: " . $sql . " - " . mysqli_error($connection) . "</div>";
    }
  }

  $sql = "SELECT NAME, NOTE FROM nagistaja_eksam ORDER BY NAME, MODIFIED";
  $exec = mysqli_query($connection, $sql);
  while($row = mysqli_fetch_assoc($exec)) {
    echo "<div class=\"panel panel-default\"><div class=\"panel-body\">" . $row['NAME'] . " - " . $row['NOTE'] . "</div></div>";
  }

  getTotalNotes();

  ?>

</div>

<script src="js/scripts.js"></script>
<script src="js/jquery-3.2.1.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
