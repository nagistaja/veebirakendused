<?php
function connectDB(){
    global $connection;
    $host="localhost";
    $user="test";
    $pass="t3st3r123";
    $db="test";
    $connection = mysqli_connect($host, $user, $pass, $db) or die("Conniecting database failed- ".mysqli_error());
    mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Unable to set utf-8 encoding - ".mysqli_error($connection));
}

function getTotalNotes(){
    global $connection;    
    $sqlCount = "SELECT COUNT(*) FROM nagistaja_eksam";
    $exec = mysqli_query($connection, $sqlCount);
    $noteCount = mysqli_fetch_assoc($exec);
    print_r("<div class=\"row note-count\">Notes created: " . $noteCount['COUNT(*)'] . "</div>");
}

?>
